# veille-node-tp1

AARON ORELLLANA

TP1 – Node.js, Express et MongoDB

Pondération : 20 points
Date de remise mercredi 21 février
Objectif:
Développer une application Node.js/Express/MongoDB permettant de contrôler la
gestion d'une liste de membres

Objectifs spécifiques
Le site permettra d'accéder les pages:
    - Accueil
    - Liste des membres
    - Profil d'un membre

Les fonctionnalités à développer
    • Génération aléatoire d'une liste de membres
    • Destruction de la liste de membres
    • Ajouter un membre
    • Modifier un membre
    • Détruire un membre
    • Rechercher un membre
    • Trier une liste de membre selon différents critères
    • Changement de la langue d'affichage du site: Un lien permettra de passer «tuggle»
    de l'anglais au français.

Intégration
    • Utiliser LESS pour le développement des feuilles de style
    • Les préfixes seront ajoutés dans les fichiers CSS par «autoPrefixe» de sublime
    • Un fichier «normalize.css » sera utilisé
    • Le site devra s'adapter aux différentes plates-formes : téléphone mobile, tablette,
    portable et ordinateur de bureau.
    • La mise en page du site sera entièrement contrôlé par des «Flexbox» et utilisera des
    media queries
    • Développer un système de «gabarit» (template) en utilisant le langage de template

EJS
    • Le système de gabarit devra être modulaire et contiendra au minimum des
    composants pour :
        o Entete
        o Menu
        o Tableau
        o Pied de page
        • Le dépôt final devra contenir un minimum de 30 commits

Remise
    • Un dépôt «veille-node-tp1» sur github
    • Démonstration en classe du fonctionnement de votre application

Barème de correction (sur 20 points)
    • Les fonctionnalités (8 points)
    • L'utilisabilité (3 points)
    o L'adaptabilité multi plates-formes
    o Qualité du design
    o Utilisabilité de l'interface
    • Organisation de la codification Node.js/Express (3 points)
    o Commentaires, nom de variable, organisation des routes
    o Utilisation d'ES6 (let, const et =>)
    o Organisation des modules/fonctions
    o Bonne utilisation des fonctions MongoDB
    • Qualité de l'intégration (HTML/CSS/LESS/JS (3 points)
    o Organisation de la codification LESS
    o Organisation des gabarits EJS
    o Organisation de la codification JavaScript client
    • Minimum de 30 «commit» (3 points)
    o remise sur le dépôt veille-node-tp1

